---
title: tl;dr
---

{{% notice info %}}
Deutsche Version: [tl;dr](../tldr.de)
{{% /notice %}}

- [Assembly video and instructions](../userguide/assembly) - Assemble your card10
- [app](/app) - Companion app for Android and iOS
- [personal state](/ps)
- [Interhacktions](/interhacktions) - a guide to making apps
- [Hatchery](https://badge.team/badge/card10) - the card10 app store, publish your app
- [Hardware](http://git.card10.badge.events.ccc.de/card10/hardware) - hardware repo with docs and specs
- [Hardware Overview](/hardware)
- [USB-C](/hardware/usbc)
- [Firmware](http://git.card10.badge.events.ccc.de/card10/firmware) - firmware repo
- [Firmware Docs](https://firmware.card10.badge.events.ccc.de/) - rendered version of the firmware docs
- [LogBook](/logbook) - records of travellers

