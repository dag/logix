---
title: Home
---
{{< figure src="/media/watchface.jpg" link="/media/watchface.jpg" title="The card10 with its default watch face" width="300px" class="floatright">}}

# card10

**card10** (From Ancient Greek καρδία (kardía, "heart")) is the name of the badge for the 2019 [Chaos Communication Camp](https://events.ccc.de/camp/2019/wiki/Main_Page).

It features an ECG sensor, Bluetooth Low Energy, an optical pulse sensor, a complete IMU with magnetometer and an environemental sensor.

# News
## Firmware Update: Pandemic Potato (as of 2020-12-04)
It is very easy to [update your card10 firmware](/firmware/firmwareupdate) with just a USB-C cable. The update instructions
also contain a list of the existing firmware releases.

## rC3 is coming!
What role(s) can play card10 in a distributed, remote conference? Brainstorming about connectivity, functionalities, interactions: [card10 at rC3](/events/card10_at_rc3)

### [Getting started](/tutorials/gettingstarted) 
You just received your card10, what now? The [Getting started](/tutorials/gettingstarted) page has some advice how to assemble and use the card10.

### [First interhacktions](/interhacktions/firstinterhacktions)
After playing around with card10 a bit, you want to learn how to write your own software. Even if you have never programmed before, it is super easy to make some LEDs blink on card10 :)

### [tl;dr](/faq/tldr)
Spare me the details just give me the links to the docs!

### Community
  - [IRC](https://en.wikipedia.org/wiki/Internet_Relay_Chat): [`freenode.net#card10badge`](ircs://chat.freenode.net:6697/card10badge)
  - mirrored to [Matrix](https://en.wikipedia.org/wiki/Matrix_(protocol)) (mirror): [`#card10badge:asra.gr`](https://matrix.to/#/#card10badge:asra.gr)
  - [GitLab: `https://git.card10.badge.events.ccc.de`](https://git.card10.badge.events.ccc.de/explore/groups/)

You can [browse](https://git.card10.badge.events.ccc.de/card10/logix) this wiki's source and [read more](/firmware/gitlab) about our GitLab instance.

### Social Media
  - [`@card10badge@chaos.social`](https://chaos.social/@card10badge)
  - [`twitter.com/card10badge`](https://twitter.com/card10badge)

### [FAQ](/faq)

### Events
  - Events [archive](/events).


### News Archive

## Hello 36C3

Our assembly is here: [c3nav](https://36c3.c3nav.de/l/card10/@0,491.08,374.33,5)

Please stop by at any time :)


We are working on a workshop schedule and post it on the [workshop page](/events/workshops/)

## Camp was wonderful, see you at 36C3
if you still have a voucher (white or blue), bring it to congress and you might be able to redeem it against one of the card10s we are reparing in the meantime.

No sale of card10s!

## Got a card10 you don't use or need one for your next project?
The card10 exchange point is [here](/other/card10exchange)


