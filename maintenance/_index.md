---
title: Maintenance & Repair
weight: 20
---

An important aspect of open hardware is that you have the information you need so you can fix it - if you have the skills to do so.
In this section hopefully we can collectively gather information and tutorials so all of us can enjoy our card10s for as long as possible!

To prevent the display from cracking consider putting your card10 into a [case](/userguide/cases). This is likely the most important thing to do so your card10 has a long life.
