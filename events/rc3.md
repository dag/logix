---
title: card10 rC3 Assembly
---

We will have an assembly in the cellar of the muccc at rC3:

## Location and Time

- Location: [rc3 cellar](https://test.visit.at.wa-test.rc3.cccv.de/_/global/raw.githubusercontent.com/muccc/rc3test/master/main.json) Attention: Link subject to change
- Times:  Day 1 - Day 4
- Capacity: virtual


