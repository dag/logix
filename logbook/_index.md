---
title: Logbook
weight: 60
---

## General 
We got some log entries from travelers that have been to this years camp. As far as we know there will be a badge, people call it card10 and "wear" it on one of their arms and connect via BLE.
It is documented that we will have been developing a wide range of so called "interhacktions" (loadables) for card10. A lot of the villages at the camp will have so called GIGAs, BLE Devices to spread new interhacktions, host games and connect to travelers.
To not ruin the future by hiding information that was given to us, we started a wiki (CARD10LOGIX) for everyone to collaborate on ideas for card10.

what travelers will write down in their log-entry at cccamp 2019 will be developed from now on by all of us.


## Logbooks
### [card10-camp-travelers](/logbook/card10-camp)
### [personal time, wristband, individual design](/logbook/i-o-time-wristband-individual-design)
### [personale state LED](/logbook/personal-state-led)
### [topics near by](/logbook/topics-near-by)
### [notifications](/logbook/notifications)
