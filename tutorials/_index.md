---
title: Tutorials
---

## Getting started


## Micropython


## E-textile

* Photo tutorial for the E-textile card10 workshop at CCCam19 by plusea https://flickr.com/photos/plusea/albums/72157710505658161
* Tutorial for sewable RGB LEDs (LilyPad Pixel Board): [Sewable RGB LEDs Tutorial](/tutorials/sewable_leds)

## ECG

* ECG kit assembly https://card10.badge.events.ccc.de/tutorials/ecg_kit_assembly

## Maintenance and repairs

We dedicated a separate section of card10logix to repair work for the maintenance of your card10: https://card10.badge.events.ccc.de/maintenance
