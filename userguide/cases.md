---
title: "Cases"
weight: 70
---

The card10 is a delicate piece of hardware. Consider getting a case for your card10 to give it a long life.

There are multiple options. The simplest is to just use the wrist band to protect your card10 while transporting it. Other options are printing a case or using a laser cutter. You can even build the simplest case using scissors and the plastics from a water bottle.

If you don't own a 3D printer (and don't have access to one or know people wo do), consider using one of the many 3D printing services like Shapeways.

## Using the wristband for protection
{{< figure src="/media/ug/6-case-wb.jpg" link="/media/ug/6-case-wb.jpg" width="800px" >}}
Simply roll one part of the wristband over the top of the card10 and use the other side to wrap around the whole card10. This protects the display from cracks while transporting the card10.


## card10 cover generator
A 3D printable case which can be customizes according to your desires. You can even print a whole wristband:
{{< figure src="/media/ug/6-case-roy.jpg" link="/media/ug/6-case-roy.jpg" width="800px" >}}
 - https://niklasroy.com/card10covergenerator/index.html
 - https://card10-cover.0x1b.de/index.php
 - https://www.thingiverse.com/thing:3898846
 - https://www.thingiverse.com/make:767080


## Wooden case by Backspace Bamberg
A case which can be cut from wood and acrylic glass:
{{< figure src="/media/ug/6-case-backspace.jpg" link="/media/ug/6-case-backspace.jpg" width="800px" >}}
 - https://www.hackerspace-bamberg.de/Card10-case


## Cases made by cutting water bottles
Use scissors to cut out a small rectangle from a water bottle and screw it down. Probably the cheapest yet somehow effective "case":
{{< figure src="/media/ug/6-case-pet.jpg" link="/media/ug/6-case-pet.jpg" width="800px" >}}
 - https://twitter.com/Haze_Core/status/1165247123669078016


## Full enclosure by ratzupaltuff
{{< figure src="/media/ug/6-case-ratzupaltuff.png" link="/media/ug/6-case-ratzupaltuff.png" width="800px" >}}
{{< figure src="/media/ug/6-case-ratzupaltuff-2.png" link="/media/ug/6-case-ratzupaltuff-2.png" width="800px" >}}
 - https://github.com/ratzupaltuff/card10-case
 - https://www.thingiverse.com/thing:3769330
 - https://www.thingiverse.com/make:709697


## Original card10 case by 9r
{{< figure src="/media/ug/6-case-9r.jpg" link="/media/ug/6-case-9r.jpg" width="800px" >}}
 - https://git.card10.badge.events.ccc.de/card10/cover
 - https://www.thingiverse.com/thing:3815098


## Case by Jan Mrlth
{{< figure src="/media/ug/6-case-jan.png" link="/media/ug/6-case-jan.png" width="800px" >}}
 - https://grabcad.com/library/card10-case-1



