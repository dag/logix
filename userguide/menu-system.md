---
title: "The menu system"
weight: 40
---

## Selecting a default app
By default the `g-watch` digital watch face is the app which is started after installing the current firmware.

If you want to change that, you can use the main menu to select a new default app.

Open the main menu and navigate to the app you want to make the default app:

![ChooChoo menu item](/media/ug/3-choochoo.jpg)

**Long** press the `SELECT` button. The following menu will appear:

![Make ChooChoo default](/media/ug/3-make-default.jpg)

If you want to make the selected app your new default app, simply press `SELECT` once more. If this is not what you wanted, either press `MENU` once or select `Exit` using the `RIGHT/DOWN` key and then press `SELECT`:

![Making ChooChoo default](/media/ug/3-making-default.jpg)

From now the app you chose will be the new "Home" app. This app will be started after turning on the card10, when selecting "Home" in the main menu or when the main menu times out.

![ChooChoo running](/media/ug/3-home.jpg)
![ChooChoo running](/media/ug/3-choochoo-running.jpg)

## Changing the button layout

This is the default button layout:

[![Drawing of card10 with button names](/media/card10buttons.svg)](/media/card10buttons.svg)


You can change the button layout to use the two buttons on the right side for `UP` and `DOWN` and one of the buttons on the left side for `SELECT` in the menus:

[![Drawing of card10 with button names](/media/card10buttons-alt.svg)](/media/card10buttons-alt.svg)

To do so you have to modify (or create) the `card10.cfg` file on the card10. Add the following line to it:

```
right_scroll = true
```

More information about `card10.cfg` can be be found in the [firmware documentation](https://firmware.card10.badge.events.ccc.de/card10-cfg.html)
