---
title: Hardware
weight: 40
---

Our reconstruction of the card10 badge consists of three things:

- The [wristband](#wristband)
  - Secures the card10 and provides space for extensions. It is made of thin neoprene with a jersey coating on one side and a fluffy velco coating on the other side.
- The [Fundamental Board](#fundamental-board)
  - Hosts most sensors, the CPU, the USB-C connection, the power management and connections to the wristband
- The [Harmonic Board](#harmonic-board)
  - Hosts the user interface, a few sensors, lots of (RGB) LEDs and the battery

The two boards are connected via a board to board connector and then screwed together with M2 screws. The wristband is attached to back of the Fundamental Board with two clamps.

![Harmonic Board prototype top side](/media/hardware/low_res_jpg/card10_harmonic_board_prototype_1.jpg)


### General Properties

|                      |             |
| -------------------- |-------------|
| Weight               |  ~ 19 g       |
| Size                 | Height (without wristband): ~8-9 mm. Width: 38 mm. Length: 45 mm |
| Display              | ST7735 0.96" Full Color IPS LCD (80x160) |
| RGB LEDs             | 11 RGB LEDs facing up, 4 RGB LEDs facing down (SK9822-2020)|
| Wirstband GPIOs      | 2 GPIOs + GND + Power per side |
| Battery capacity     | 200 mAh (prototypes: 100-150 mAh) |
| CPU                  | MAX32666 (dual core Cortex-M4F at 96 MHz)   |
| RAM                  | 512 kB      |
| Flash                | 1 MB internal + 8 MB external |
| Accelerometer        | BMA400 |
| Sensor fusion        | BHI160 (accelerometer + gyroscope + sensor fusion) |
| Magnetometer         | BMM150 |
| Environmental sensor | BME680 (temperature, rel. humidity, air pressure, air quality) |
| ECG                  | MAX30001 |
| Pulse sensor         | MAX86150 (with second ECG channel) |
| Ambient light sensor | 940 nm IR LED attached to ADC |

## Wristband
![card10 reconstruction prototype](/media/hardware/low_res_jpg/card10_band_prototype.jpg)

More information to come.

## Fundamental Board


The Fundamental Board is located just above the wristband; above it comes the battery, the Harmonic Board, and the display. The Harmonic Board is connected to the Fundamental Board via a board to board connector.

Several interfaces for [interhacktions](/interhacktions/) are included on the Fundamental Board. The travelers can be alerted to an event using the vibration motor. Screws through the ECG contacts are used both for attaching the wristband and for making contact with your skin for ECG measurements. Besides its use for interhacktions, the [USB-C](/hardware/usbc/) connector is also used for charging the battery and for updating the firmware.

A PDF version of the schematic of the Fundamental Board is available [here](https://git.card10.badge.events.ccc.de/card10/hardware/blob/master/boards/Fundamental-Board/Fundamental-Board.pdf)

![landmarks](/media/landmarks-fundamental-top.png)

Fundamental Board prototype:
![Fundamental Board prototype](/media/hardware/low_res_jpg/card10_fundamental_board_prototype.jpg)
## Harmonic Board

The Harmonic Board sits on top of the Fundamental Board. It contains most of the user interface and a few sensors.

A PDF version of the schematic of the Harmonic Board is available [here](https://git.card10.badge.events.ccc.de/card10/hardware/blob/master/boards/Harmonic-Board/Harmonic-Board.pdf)

Harmonic Board prototype top side:
![landmarks](/media/landmarks-harmonic-top.png)

![Harmonic Board prototype top side](/media/hardware/low_res_jpg/card10_harmonic_board_prototype_1.jpg)

Harmonic Board prototype bottom side:
![landmarks](/media/landmarks-harmonic-bottom.png)

![Harmonic Board prototype bottom side](/media/hardware/low_res_jpg/card10_harmonic_board_prototype_2.jpg)

