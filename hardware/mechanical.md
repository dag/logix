---
title: Mechanical Data
---

Our reconstruction of the card10 badge is made of two 1 mm PCBs, spaced 5 mm apart.

- The [Fundamental Board](/hardware/#fundamental-board) is a bit larger than the [Harmonic Board](/hardware/#harmonic-board)
 - To make sewing the wristband connections easier when the card10 is fully assembled
- The wirstband is about 34-38 mm wide and 25 - 30 cm long
- The two PCBs are spaced using three 5 mm brass spacers and one 5 mm nylon spacer
- The PCBs are screwed to the spacers using eight 3 mm M2 screws

## Dimensions
### Fundamental Board
![Fundamental Board dimensions](/media/hardware/Fundamental-Board-dimensions.png)

### Harmonic Board
![Harmonic Board dimensions](/media/hardware/Harmonic-Board-dimensions.png)

## Design Files
### Fundamental Board

SVG with bottom copper layer. View is from above (through the PCB)

![Fundamental Board dimensions](/media/hardware/Fundamental-Board-dimensions.svg)

Source of truth: [KiCAD PCB File](https://git.card10.badge.events.ccc.de/card10/hardware/blob/master/boards/Fundamental-Board/Fundamental-Board.kicad_pcb)

### Harmonic Board
SVG with top copper layer. View is from above (onto the PCB)

![Harmonic Board dimensions](/media/hardware/Harmonic-Board-dimensions.svg)

Source of truth: [KiCAD PCB File](https://git.card10.badge.events.ccc.de/card10/hardware/blob/master/boards/Harmonic-Board/Harmonic-Board.kicad_pcb)
